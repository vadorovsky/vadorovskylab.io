+++
title = "About me"
slug = "about"
+++

I'm Mike, an enthusiastic open source developer, based in Berlin 🇩🇪 and interested
in:

* Crypto(graphy, currency)
  * With a particular fondness towards [Solana](https://solana.com/) as the one
    aiming for the best performance.
  * Zero-knowledge proofs, using them to provide privacy in cryptocurrency projects.
* Linux kernel
  * [Linux Security Module framework](https://www.kernel.org/doc/html/latest/admin-guide/LSM/index.html)
    and using it for development of security policies.
  * [eBPF](https://ebpf.io/), with emphasis on [LSM programs](https://aya-rs.dev/book/programs/lsm/),
    with the same goal as mentioned above.
  * [Building it with Clang/LLVM](https://clangbuiltlinux.github.io/), adoption of
    ThinLTO and other performance-improving.

Rust holds a special place in my coding heart, but I have a history of working in
C, C++, Go, and Python. I'm open to learning and maybe one day, I'll add Zig to
that list.

Currently I work mostly on following projects:

* [Light Protocol](https://lightprotocol.com/) - zero-knowledge protocol built
  on top of [Solana](https://solana.com/). Current mainnet version provides private
  transactions on Solana. Testnet version (mainnet soon!) brings the SDK for writing
  [Private Solana Programs](https://docs.lightprotocol.com/getting-started/creating-a-custom-psp).
  * [light-poseidon](https://github.com/Lightprotocol/light-poseidon) - [Poseidon hash](https://www.poseidon-hash.info/)
    implementation in Rust.
* [Aya](https://aya-rs.dev/) - library for writing [eBPF](https://ebpf.io/) programs
  in Rust.
* [ebpfguard](https://github.com/deepfence/ebpfguard) - library for writing Linux
  security policies in Rust.

In addition to these, I frequently find myself working on small projects, such as:

* [network-types](https://github.com/vadorovsky/network-types) - network protocol
  types (on Layer 2, 3, 4) represented as Rust structs. Useful for writing
  [eBPF/XDP programs](https://ebpf.io/), packet sniffers etc.
* [verstau](https://gitlab.com/vadorovsky/verstau) - symlink manager based on TOML
  configuration.
  * [dotfiles](https://gitlab.com/vadorovsky/dotfiles) repository which is using
    verstau for storing my configuration of Gentoo/Portage (USE flags, accept
    keywords, overlays etc.) and tools I use ([Helix](https://helix-editor.com/),
    [nushell](https://www.nushell.sh/) etc.).
* [unseal](https://github.com/vadorovsky/unseal) - a simple archive manager.

I'm perpetually open to exciting freelance projects, particularly those that offer
the opportunity to contribute to open source work in my areas of interest.

For collaboration or just a chat, the best ways to reach me are:

* email: <a href="mailto:vadorovsky@protonmail.com">vadorovsky@protonmail.com</a>
* Matrix: @vadorovsky:matrix.org
* Discord: vadorovsky
